"""
Module containing a Narupa client for benchmarking performance.

"""
import dataclasses
import datetime
import os
import time
from dataclasses import dataclass
from typing import Union

from narupa.core import NarupaClient
from narupa.protocol.command import GetCommandsRequest
from narupa.trajectory import FrameClient, FrameData
from pandas import DataFrame


@dataclass
class BenchmarkParams:
    """
    Parameters for benchmarking.
    """
    n_frames: int = 10


@dataclass
class FrameLogFeatures:
    """
    Features produced when sampling frame receive times.
    """
    frame_index: int
    message_size: int
    frame_receive_time: float
    timestamp: datetime.datetime

    @staticmethod
    def empty_frame():
        return DataFrame(columns=__class__.__annotations__.keys())


@dataclass
class PingLogFeatures:
    """
    Features produced when sampling ping times.
    """
    message_size: int
    ping_time: float
    timestamp: datetime.datetime

    @staticmethod
    def empty_frame():
        return DataFrame(columns=__class__.__annotations__.keys())


class FrameBenchmarkClient:
    """
    A client that connects to a server and produces data frames of profiling information
    whenever it receives a frame.
    """
    last_time: Union[None, float] = None
    current_frame_index = 0
    frame_receive_results: DataFrame
    ping_results: DataFrame

    def __init__(self):
        self.last_time = None
        self.current_frame_index = 0
        self.frame_receive_results = FrameLogFeatures.empty_frame()
        self.ping_results = PingLogFeatures.empty_frame()

    def run_frame_receive(self,
                          address,
                          port,
                          params: BenchmarkParams = None,
                          ping=True,
                          ping_interval=0.5):
        """
        Connects to the given server address and port, receives the given number of frames
        specified in params and logs timing information `frame_receive_results`.
        If the parameter `ping` is set, it will also continually ping the server and log timing information
        of that into `ping_results`.

        :param address: Address to connect to.
        :param port: Port to connect to.
        :param params: Benchmark parameters.
        :param ping_interval: Interval at which to ping
        :param ping: Whether to ping the server as well as receive frames.
        """
        if not params:
            params = BenchmarkParams()
        with FrameClient.insecure_channel(address=address, port=port) as frame_client:
            frame_client.subscribe_frames_async(self._on_frame_received)
            while self.current_frame_index < params.n_frames:
                if ping:
                    self._ping_server(frame_client)
                time.sleep(ping_interval)

    def write_to_file(self, path):
        timestamp = _get_timestamp()
        _write_results_to_file(path, self.frame_receive_results, 'frames', timestamp)
        _write_results_to_file(path, self.ping_results, 'pings', timestamp)

    def _log_ping(self, elapsed_time, reply):
        sample = PingLogFeatures(message_size=reply.ByteSize(),
                                 ping_time=elapsed_time,
                                 timestamp=datetime.datetime.now())
        self.ping_results = append_dataclass_to_dataframe(self.ping_results, sample)

    def _log_frame(self, frame_index, frame, elapsed_time):
        sample = FrameLogFeatures(frame_index=frame_index,
                                  message_size=frame.raw.ByteSize(),
                                  frame_receive_time=elapsed_time,
                                  timestamp=datetime.datetime.now())
        self.frame_receive_results = append_dataclass_to_dataframe(self.frame_receive_results, sample)

    def _on_frame_received(self, frame_index: int, frame: FrameData):
        current_time = time.perf_counter()
        if self.last_time is not None:
            elapsed_time = (current_time - self.last_time) * 1000
            self._log_frame(frame_index, frame, elapsed_time)
            self.current_frame_index += 1
        self.last_time = current_time

    def _ping_server(self, client: NarupaClient):
        """
        Application level 'ping' that asks the server for available commands and logs the response time.
        """
        current_time = time.perf_counter()
        reply = client._command_stub.GetCommands(GetCommandsRequest())
        elapsed_time = (time.perf_counter() - current_time) * 1000
        self._log_ping(elapsed_time, reply)


def append_dataclass_to_dataframe(df: DataFrame, instance: dataclass):
    return df.append(dataclasses.asdict(instance), ignore_index=True)


def _write_results_to_file(path, df: DataFrame, result_name, timestamp):
    output_dir = os.path.join(path, result_name)
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    file_path = os.path.join(output_dir, f'{result_name}_{timestamp}.csv')
    df.to_csv(file_path, index=False, float_format='%.3f')


def _get_timestamp():
    now = datetime.datetime.now()
    timestamp = f'{now:%Y_%m_%d__%H_%M_%S}_{int(now.microsecond / 10000):02d}'
    return timestamp
