"""
Command line interface for running benchmarking client.
"""
import argparse
import datetime
import os
import textwrap
import time

from client import BenchmarkParams, FrameBenchmarkClient


def handle_user_arguments(args=None) -> argparse.Namespace:
    """
    Parse the arguments from the command line.

    :return: The namespace of arguments read from the command line.
    """
    description = textwrap.dedent("""\
    Run Narupa frame benchmark client.
    """)
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('address', type=str, default='localhost')
    parser.add_argument('-p', '--port', type=int, default=38801)
    parser.add_argument(
        '-n', '--n_frames', type=int, default=1000,
        help='How many frames to receive before ending data collection.'
    )
    parser.add_argument('-o', '--output', type=str, default='./frame_receive_benchmark',
                        help='Output directory')
    arguments = parser.parse_args(args)
    return arguments


def main():
    """
    Entry point for the command line.
    """
    arguments = handle_user_arguments()
    n_frames = arguments.n_frames
    params = BenchmarkParams(n_frames)
    address = arguments.address
    port = arguments.port

    print(f'Connecting to {address}:{port} to receive (around) {n_frames} frames.')

    client = FrameBenchmarkClient()
    client.run_frame_receive(address, port, params)

    print(f'Collection done, outputting to {arguments.output}')
    client.write_to_file(arguments.output)


if __name__ == '__main__':
    main()
