## Narupa 2 Benchmark Client

Some simple scripts for gathering performance data from a 
client's perspective with Narupa 2 in python. 

## Running the benchmark 

A command line interface exists that will connect to a server
receive `n` frames, do a bunch of pings, and output some csv files. 
  
```
python cli.py address -p 54321 -n 10000
```

## Analysing the results 

The notebook [plot_benchmark.ipynb](plot_benchmark.ipynb)
shows some example analysis on some data collected between a client and server
running an OpenMM Nanotube at ETH.
to produce distributions and percentile plots.

## Potential things to add 

* Connect multiple clients at the same time to see if performance changes.
* A custom server that adds more performance metadata such as compute time
* gRPC level interceptors so we can distinguish between application level code and the transport.
